function ShopController($scope,$location, ShopService,UserService){
	$scope.init = function(part){
		if(part == 'add-shop'){
			$scope.user_shop_list = UserService.UserListShop.query(function(){});
			$scope.info_shop = {};
			$scope.show_user_list = 0;
			$scope.select_user = function(user){
				$scope.info_shop.user_name = user.fname+' '+user.lname;
				$scope.info_shop.user_id = user.id;
				$scope.show_user_list = 0;
			}
			$scope.post_shop = function(){
				var shop = new ShopService.Shop($scope.info_shop);
				shop.$save();
			}
		}
		if(part == 'main'){
			$scope.open_shop = function(id){
				ShopService.OpensShop.get({id:id},function(d){
					$scope.shop_open=d;
				})
			};
			$scope.list_shops = ShopService.ShopList.query(function(){});
		}
	}
}