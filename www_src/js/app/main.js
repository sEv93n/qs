function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function GlobalController($scope, $http) {
    // $scope.menu = srv.get_menu();
    // $scope.balans = srv.get_balans();
    $http({
        method: 'GET',
        url: '/user/info'
    }).success(function(data, status, headers, config) {
        if (data) {
            $scope.balans = data.balans;
            console.log($scope.balans);
        } else {
            console.log('FAIL');
        }
    }).error(function(data, status, headers, config) {});

}

function StartPage($scope, $http) {
    $scope.now_show = "text";
    $scope.form_data = {
        fields: {
          fname: '',
          lname: '',
          email: '',
          country:'',
          city: '',
          phone:'',
          password: '',
          repassword: '',
          referal:'',
          icheck: true

        },
        send_auth: function() {
            var $this = this;
            $http({
                method: 'POST',
                url: '/user/auth',
                data: $this.fields
            }).success(function(data, status, headers, config) {
                if (data.result) {
                    document.location.href = "/";
                } else {
                    console.log('FAIL');
                }
            }).error(function(data, status, headers, config) {});
        },
        send_reg: function() {
            var $this = this;
            $http({
                method: 'POST',
                url: '/user/reg',
                data: $this.fields
            }).success(function(data, status, headers, config) {
                if (data.result) {
                    document.location.href = "/";
                } else {
                    console.log('FAIL');
                }
            }).error(function(data, status, headers, config) {});
        }
    };
}

angular
  .module('quickstart',['ngResource', 'ngRoute', 'ngSanitize'])
  .factory('UserService', UserService)
  .controller('UserController', UserController)
  .controller('UserControllerAdmin',UserControllerAdmin)
  .factory('ShopService', ShopService)
  .controller('ShopController', ShopController)
  .factory('ItemService', ItemService)
  .controller('ItemController', ItemController)
  .config(function($interpolateProvider) {
        $interpolateProvider.startSymbol('{$');
        $interpolateProvider.endSymbol('$}');
    });
