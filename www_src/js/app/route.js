app.config(function($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '/html/start.html',
            controller: function(){}
        })
        .when('/no_auth', {
            templateUrl: '/html/start.html',
            controller: function(){
                document.location.href = "/";
            }
        })
        .when('/page/1', {
            templateUrl: '/html/page_1.html',
            controller: function(){}
        })
        .when('/page/2', {
            templateUrl: '/html/page_2.html',
            controller: function(){}
        })
        .when('/page/3', {
            templateUrl: '/html/page_3.html',
            controller: function(){}
        })
        .when('/page/4', {
            templateUrl: '/html/page_4.html',
            controller: function(){}
        })
        .when('/page/5', {
            templateUrl: '/html/page_5.html',
            controller: function($scope,srv){
                $scope.tables = srv.get_tables();
                $scope.profit = srv.get_profit();
            }
        })
        .when('/page/6', {
            templateUrl: '/html/page_6.html',
            controller: function(){}
        })
        .when('/page/7', {
            templateUrl: '/html/page_7.html',
            controller: function(){}
        })
        .when('/page/8', {
            templateUrl: '/html/page_8.html',
            controller: function(){}
        })
        .when('/page/9', {
            templateUrl: '/html/page_9.html',
            controller: function(){}
        })
        .when('/page/10', {
            templateUrl: '/html/page_10.html',
            controller: function(){}
        })
        .when('/page/11', {
            templateUrl: '/html/page_11.html',
            controller: function(){}
        })
        .otherwise({
            redirectTo: '/'
        })
    // $locationProvider.html5Mode(true);
});