function UserService($http, $resource) {
    var
        srv = {
            UserProfile: $resource('/user/info'),
            UserList: $resource('/user/list/json'),
            UserListShop: $resource('/user/list/json.shop')
        };
    return srv;
}