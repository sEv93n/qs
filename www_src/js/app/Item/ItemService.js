function ItemService($http, $resource) {
    var
    	_url_shop_upload = '/item/upload_file'
        upload = function(file,item_id,callback) {
            var form_data = new FormData();
            form_data.append("file", file);
            form_data.append("item_id", item_id);
            var xhr = new XMLHttpRequest(),
                _a = this;

            xhr.onload = function() {
                var d = JSON.parse(this.responseText);
                if (callback) callback(d);
            }
            xhr.addEventListener("error", function(evt) {
                console.log('UpLoadFiles', status);
            }, false);
            xhr.open("POST", _url_shop_upload);
            xhr.send(form_data);
        },
        srv = {
            Item:$resource('/item/new'),
            UploadFile: upload,
        };
    return srv;
}
