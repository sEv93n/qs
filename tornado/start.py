#!/usr/bin/python
#-*-coding: utf8

import tornado.ioloop
import tornado.httpserver

from Server.QuickStart import Application as QS

__author__ = 'eshreder'
__email__ =  'eshreder@gmail.com'

def main():
	__port_server = 9991
	__listen_ip = '0.0.0.0'
	http_server = tornado.httpserver.HTTPServer(QS())
	http_server.listen(__port_server, address=__listen_ip)
	print "Старт сервера..."
	tornado.ioloop.IOLoop.instance().start()

if __name__ == '__main__':
	main()