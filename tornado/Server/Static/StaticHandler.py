#-*-coding: utf8

import tornado.template
from Server.lib.BaseHandler import BaseHandler
from Server.User.UserModel import UserModel
from Server.lib.AccessRule import AccessRule
from os import path

class StaticHandler(BaseHandler):
	def get(self,a):
		if(not self.get_current_user()):
			self.auth()
		else:
			self.index()
		# print self.request.headers
		# self.auth()

	def index(self):
		# print AccessRule(self).get_menu()
		self.render(	"index.html",
						user_count=UserModel.count_registars(),
						menu=AccessRule(self).get_menu(),
						user_id = self.get_current_user(),
						**self.settings)
	
	def auth(self):
		self.render(	"auth.html",
						user_count=UserModel.count_registars(),
						menu=AccessRule(self).get_menu(),
						user_id = self.get_current_user(),
						**self.settings)