#!/usr/bin/python
#-*-coding: utf8

import os.path
import tornado.httpserver
import tornado.web

# from json import loads

from Server.Static.StaticHandler import StaticHandler
from Server.User.UserController import UserHandler
from Server.Shop.ShopController import ShopHandler
from Server.Item.ItemHandler import ItemHandler

class Application(tornado.web.Application):
    def __init__(self):
        settings = dict(
            main_title=u"QuickStore.",
            base_path=os.path.join(os.path.dirname(__file__),'..'),
            template_path=os.path.join(os.path.dirname(__file__), "..", "templates"),
            static_path=os.path.join(os.path.dirname(__file__),'../..', "www_src"),
            data_path=os.path.join(os.path.dirname(__file__),'../','data'),
            temp_data_path=os.path.join(os.path.dirname(__file__),'../','temp'),
            orm_db_path=os.path.join('sqlite://',os.path.dirname(__file__),'../','data','orm_in_detail.sqlite'),
            cookie_secret="__SUPER_SECRET_INFO__01",
            login_url="/",
            debug=True,
            version=u"0.1",
            controller_js=u""
        )

        handlers = [
            (r'/user[\/]*(.*)', UserHandler, {}),
            (r'/shop[\/]*(.*)', ShopHandler, {}),
            (r'/item[\/]*(.*)', ItemHandler, {}),
            (r'/()', StaticHandler, {}),
            (r'/(.*)', tornado.web.StaticFileHandler, {'path': settings['static_path']}),
        ]

        tornado.web.Application.__init__(self, handlers, **settings)
