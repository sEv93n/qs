#-*-coding: utf8

import tornado.web
import json
from hashlib import sha224
from Server.lib.SQLAlchemy_Main import UserDB

class BaseHandler(tornado.web.RequestHandler):
    """
     Базовый класс для контроллеров
    """
    def initialize(self,**argv):
        self.argv = argv;

    def decode_argument(self,v,name=None):
        try:
            return v.decode('cp1251').encode('utf8')
        except UnicodeDecodeError:
            raise tornado.web.HTTPError(400, "Invalid unicode in %s: %r" %(name or "url", v[:40]))

    def get(self,url):
        """
          Базовая обработка GET запросов.
          Если есть функция с именем act_get_[url[0]] - выполнить ее.
        """
        url = url.split('/')
        if hasattr(self, '_'.join(['act_get',url[0]]).rstrip('_')):
            func = getattr(self, '_'.join(['act_get',url[0]]).rstrip('_'))
            func(url)
        else:
            raise tornado.web.HTTPError(404)

    def post(self,url):
        """
          Базовая обработка POST запросов
          Если есть функция с именем act_POST_[url[0]] - выполнить ее.
        """
        url = url.split('/')
        if hasattr(self, '_'.join(['act_post',url[0]]).rstrip('_')):
            func = getattr(self, '_'.join(['act_post',url[0]]).rstrip('_'))
            func(url)
        else:
            raise tornado.web.HTTPError(404)

    def get_current_user(self):
        """
          Проверка авторизации пользователя
          Если пользователь авторизован - вернет имя
            иначе вернет занчение None
        """
        uID =   self.get_secure_cookie("id")
        UA =    self.get_secure_cookie("aid")
        _UA =   sha224("%s-%s"%(uID,self.request.headers['User-Agent'])).hexdigest()

        if(not uID): return None
        if(_UA != UA): return None

        _user = UserDB.by_id(uID)
        if(not _user): return None
        self.user = _user
        return uID

    def json(self,data):
        """
         Отдать данные в JSON. С указанием Content-Type application/json
        """
        self.set_header("Content-Type", "application/json;")
        self.write(json.dumps(data))

    def data_from_body_request(self):
        return json.loads(self.request.body)

    @staticmethod
    def gen_error(num):
        raise tornado.web.HTTPError(num)

    def set_type_css(self):
        self.set_header("Content-Type", "text/css;")

    def set_type_js(self):
        self.set_header("Content-Type", "application/javascript;")

    def set_type_ttf(self):
        self.set_header("Content-Type", "application/x-font-ttf")

    def set_type_imgs(self):
        self.set_header("Content-Type", "image/jpg;")

    def set_type_tiff(self):
        self.set_header("Content-Type", "image/tiff;")

    def set_type_wav(self):
        self.set_header("Content-Type", "audio/wav;")