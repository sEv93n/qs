#!/usr/bin/python
#-*-coding: utf8
__author__ = 'eshreder'

from sqlalchemy import Column, DateTime, String, Integer, ForeignKey, func
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.sqlite import \
    BLOB, BOOLEAN, CHAR, DATE, DATETIME, DECIMAL, FLOAT, \
    INTEGER, NUMERIC, SMALLINT, TEXT, TIME, TIMESTAMP, \
    VARCHAR

from hashlib import sha224 
from datetime import datetime
from random import choice
import time
import ldap 
import json

Base = declarative_base()

class UserDB(Base):
	__tablename__ = 'users'
	id = Column(INTEGER, primary_key=True)
	approved = Column(BOOLEAN)
	approved_time = Column(DATETIME)

	email = Column(VARCHAR(length=64))
	phone = Column(VARCHAR(length=18))
	country = Column(VARCHAR(length=32))
	city = Column(VARCHAR(length=32))
	fname = Column(VARCHAR(length=32))
	lname = Column(VARCHAR(length=32))
	referal = Column(INTEGER)

	password = Column(VARCHAR(length=64))
	salt = Column(VARCHAR(length=32))

	types = Column(VARCHAR(length=32))

	auth = False

	@staticmethod
	def count():
		_s = session()
		_count = _s.query(func.count(UserDB.id)).first()[0]
		return _count

	@staticmethod
	def by_email(email):
		_s = session()
		_user = _s.query(UserDB).filter(UserDB.email == email).first()
		if _user: _user.types = _user.types.split('|')
		return _user

	@staticmethod
	def by_phone(phone):
		_s = session()
		_user = _s.query(UserDB).filter(UserDB.phone == phone).first()
		if _user: _user.types = _user.types.split('|')
		return _user

	@staticmethod
	def by_id(id):
		_s = session()
		_user = _s.query(UserDB).filter(UserDB.id == id).first()
		if _user: _user.types = _user.types.split('|')
		# _user.types.append(u'a')
		return _user		

	@staticmethod
	def create_user(data):
		_s = session()
		_user = UserDB()
		_salt = ''.join(map(lambda x: choice("1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM,.><;\|}{][=-+_!@#$%^&*()"), range(32) ))
		_user.approved = False
		_user.email = data['email']
		_user.phone = data['phone']
		_user.country = data['country']
		_user.city = data['city']
		_user.fname = data['fname']
		_user.lname = data['lname']
		_user.referal = data['referal']
		_user.salt = _salt
		_user.password = sha224("%s%s%s"%(_salt, data['password'], _salt)).hexdigest()

		_user.types = 'u'

		_s.add(_user)
		_s.commit()

		return _user

	@staticmethod
	def get_type_user(id):
		_s = session()
		_user = _s.query(UserDB).filter(UserDB.id == id).first()
		if(_user):
			# _user.types = 'u|m|a'
			return _user.types.split('|')
		return []

	@staticmethod
	def list_by_id(offset=0, desk=False):
		_out_list = []
		_s = session()
		_users = _s.query(UserDB).offset(offset).all()
		for user in _users:
			_out_list.append({
					"id":user.id,
					"fname":user.fname,
					"lname":user.lname,
					"types":user.types.split('|'),
					"email":user.email,
					"phone":user.phone
				})

		return _out_list

	@staticmethod
	def list_with_shop():
		_out_list = []
		_s = session()
		_users = _s.query(UserDB).filter(UserDB.types.like("%s%")).all()
		for user in _users:
			_out_list.append({
					"id":user.id,
					"fname":user.fname,
					"lname":user.lname,
					"types":user.types.split('|'),
					"email":user.email,
					"phone":user.phone
				})

		return _out_list

	@staticmethod
	def change_by_id(id,data):
		_s = session()
		_user = _s.query(UserDB).filter(UserDB.id == id).first()
		if(_user):
			_types = data.get('types', None)

			if(_types):
				_user.types = _types
			_s.add(_user)
			_s.commit()
			return 0
		return 1

	def auth(self,password):
		_salt_password = sha224("%s%s%s"%(self.salt, password, self.salt)).hexdigest()
		if(_salt_password == self.password):
			self.auth = True
			return True

class ShopDB(Base):
	__tablename__ = 'shop'
	id = Column(INTEGER, primary_key=True)
	title = Column(VARCHAR(length=32))
	descript = Column(TEXT)
	user_id = Column(Integer, ForeignKey('users.id'))
	user = relationship("UserDB", backref="shops")


	@staticmethod
	def by_title(title):
		_s = session()
		_shop = _s.query(ShopDB).filter(ShopDB.title == title).first()
		return _shop

	@staticmethod
	def by_id(id):
		_s = session()
		_shop = _s.query(ShopDB).filter(ShopDB.id == id).first()
		return _shop

	@staticmethod
	def register(data):
		title = data.get('title','')
		_shop = ShopDB.by_title(title)
		if(not _shop):
			_s = session()
			_shop = ShopDB()
			_shop.descript = data.get('descript',None)
			_shop.title = data.get('title',None)
			_shop.user_id = data.get('user_id',None)
			_s.add(_shop)
			_s.commit()
		return _shop

	@staticmethod
	def list():
		_s = session()
		_shops = _s.query(ShopDB).all()
		return _shops

class ItemDB(Base):
	__tablename__ = 'items'
	id = Column(INTEGER, primary_key=True)
	title = Column(VARCHAR(length=32))
	descript = Column(TEXT)
	price_a = Column(INTEGER)
	price_b = Column(INTEGER)

	shop_id = Column(Integer, ForeignKey('shop.id'))
	shop = relationship("ShopDB", backref="items")

	@staticmethod
	def create_new(data):
		try:
			_item = ItemDB()
			_item.title = data.get('title','Noname')
			_item.descript = data.get('descript','NoDescript')
			_item.price_a = int(data.get('pricea',0))
			_item.price_b = int(data.get('priceb',0))
			_item.shop_id = int(data.get('shop_id'))
			_s = session()
			_s.add(_item)
			_s.commit()
			return _item
		except Exception, x:
			print "Error", x
			return None

	@staticmethod
	def by_id(id):
		_s = session()
		_item = _s.query(ItemDB).filter(ItemDB.id == id).first()	
		return _item



from sqlalchemy import create_engine

engine = create_engine('sqlite:///db.sqlite')

from sqlalchemy.orm import sessionmaker
from sqlalchemy import func
session = sessionmaker()
session.configure(bind=engine)
Base.metadata.create_all(engine)