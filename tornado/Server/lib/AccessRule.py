#-*-coding: utf8

from Server.lib.SQLAlchemy_Main import UserDB

class AccessRule:
	default_menu = [
			{'access':['u'],'url': '/', 'title': 'Моя страница'},
			{'access':['u'],'url': '/messages', 'title': 'Сообщения'},
			{'access':['u'],'url': '/shop', 'title': 'Магазин'},
			{'access':['u'],'url': '/statistics', 'title': 'Статистика', 'new': True},
			{'access':['u'],'url': '/tables', 'title': 'Совместные покупки', 'new': True},
			{'access':['u'],'url': '/', 'title': 'Акции'},
			{'access':['u'],'url': '/news', 'title': 'Новости'},
			{'access':['u'],'url': '/', 'title': 'Развлечения'},
			{'access':['u'],'url': '/history', 'title': 'История операций'},
			{'access':['u'],'url': '/blogs', 'title': 'Блог', 'new': True},
			{'access':['a'],'url': '/user/list', 'title': 'Пользователи'}
		]
	def __init__(self,tornado_headler):
		self.t_self = tornado_headler

	def get_menu(self):
		out_menu = []
		uid = self.t_self.get_current_user()
		access_levels = set(UserDB.get_type_user(uid))
		for item in self.default_menu:
			need = set(item.get('access',[]))
			if(need&access_levels):
				out_menu.append(item)
		return out_menu