#-*-coding: utf8

from os import popen,path
from hashlib import sha224
from shutil import copyfile
from PIL import Image

from Server.lib.SQLAlchemy_Main import UserDB

class UserModel():
	__min_length_password = 6
	__path_user = 'user'
	def __init__(self):
		pass

	@staticmethod
	def count_registars():
		c = UserDB.count()
		return c

	@staticmethod
	def register_user(data):
		#CHECK_DATA
		if(UserDB.by_phone(data.get('phone','0'))):
			return {"result":False, "comment":"Телефон уже зарегистрирован"}
		if(UserDB.by_email(data.get('email','0'))):
			return {"result":False, "comment":"Email уже зарегистрирован"}

		_c_pass = UserModel.check_field_password(data.get('password',''),data.get('repassword',''))
		if(not _c_pass["result"]):
			return _c_pass

		user = UserDB.create_user(data)

	@staticmethod
	def auth_user(data):
		password = data.get('password','')
		login = data.get('email','')
		
		_user = UserDB.by_email(login)
		if(_user and _user.auth(password)):
			return {"result":_user,"comment":''}
		_user = UserDB.by_phone(login)
		if(_user and _user.auth(password)):
			return {"result":_user,"comment":''}

		return {"result":False, "comment":"Неверный логин или пароль"}

	@staticmethod
	def get_userpick(id,data_path):
		path_userpick = "%s/%s/%s/userpick.jpg"%(data_path,UserModel.__path_user,id)
		if(path.exists(path_userpick)):
			return open(path_userpick).read()
		return ""

	@staticmethod
	def user_profile(id):
		out_info = {}
		_user = UserDB.by_id(id)
		if(_user):
			out_info = {
				"fname": _user.fname,
				"lname": _user.lname,
				"country": _user.country,
				"city": _user.city,
				"email": _user.email,
				"phone": _user.phone,
				"referal": _user.referal,

				"count_prod": 10,
				"out_money": 3004,
				"in_money": 8372,
				"my_cli": 23,
				"all_cli": 102
			}
		return out_info

	#{u'city': u'Chelyabinsk', 
	# u'repassword': u'1qw2!QW@', 
	# u'referal': u'', 
	# u'country': u'Russia', 
	# u'lname': u'Shreder', 
	# u'phone': u'89120909090', 
	# u'fname': u'Evgenii', 
	# u'icheck': True, 
	# u'password': u'1qw2!QW@', 
	# u'email': u'eshreder@gmail.com'}

	@staticmethod
	def check_field_password(password,repassword):
		if(len(password)<=UserModel.__min_length_password):
			return {"result": False, "comment":"Короткий пароль"}
		if(password != repassword):
			return {"result": False, "comment":"Пароли не совпадают"}
		return{"result": True}


	@staticmethod
	def list(offset):
		_list = UserDB.list_by_id(offset)
		return _list

	@staticmethod
	def list_shop():
		_list = UserDB.list_with_shop()
		return _list

	@staticmethod
	def change_user(data):
		_id=data.get('id',None)
		if(not _id): return 0
		if(data.get('types',None)):
			if(type(data['types'])==list):
				data['types'] = '|'.join(map(lambda x: x[0],data['types']))
		UserDB.change_by_id(_id, data)







