#-*-coding: utf8

import tornado.template
import tornado.web
from Server.lib.BaseHandler import BaseHandler
from Server.User.UserModel import UserModel
from Server.lib.AccessRule import AccessRule

from hashlib import sha224
class UserHandler(BaseHandler):
	def act_post_reg(self,url):
		data = self.data_from_body_request()
		result = UserModel.register_user(data)
		self.json(result)

	def act_get_logout(self,url):
		self.set_secure_cookie('id','')
		self.set_secure_cookie('aid','')
		self.redirect('/')

	def act_post_auth(self,url):
		data = self.data_from_body_request()
		result = UserModel.auth_user(data)

		if(not result['result']):
			self.json(result)
		else:
			self.set_secure_cookie('id',"%d"%result['result'].id)
			self.set_secure_cookie('aid',sha224("%d-%s"%(result['result'].id,self.request.headers['User-Agent'])).hexdigest())
			self.json({"result":result['result'].id, "comment":""})

	@tornado.web.authenticated
	def act_get_info(self,url):
		url = url[1:]
		if(len(url)>0):
			if(url[0]=='userpick'):
				self.set_type_imgs()
				self.write(UserModel.get_userpick(self.get_current_user(), self.settings['data_path']))
		else:
			uid = self.get_current_user()
			self.json(UserModel.user_profile(uid))

	@tornado.web.authenticated
	def act_get_list(self,url):
		url = url[1:]
		if(len(url)>0):
			if(url[0]=='json'):
				_offset = int(self.get_argument('offset',"0"))
				_list = UserModel.list(_offset)
				self.json(_list)
			elif(url[0]=='json.shop'):
				_list = UserModel.list_shop()
				self.json(_list)
		else:
			self.render(	"user.list.html",
							user_mode=self.user.types,
							menu=AccessRule(self).get_menu(),
							user_id=self.get_current_user(),
							**self.settings)

	@tornado.web.authenticated
	def act_post_list(self,url):
		if('a' not in self.user.types):
			self.gen_error(403)
		url = url[1:]
		if(len(url)>0):
			if(url[0]=='json'):
				data = self.data_from_body_request()
				UserModel.change_user(data)
			elif(url[0]=='json.shop'):
				pass


			