#-*-coding: utf8

from os import popen,path
from hashlib import sha224
from shutil import copyfile
from PIL import Image

from Server.lib.SQLAlchemy_Main import ShopDB

class ShopModel():
	@staticmethod
	def create(data):
		return ShopDB.register(data)
	

	@staticmethod
	def list():
		_list = ShopDB.list()
		_out_list = []
		for s in _list:
			_out_list.append({
				"id":s.id,
				"title":s.title,
				"user_id":s.user.id
			})
		return _out_list

	@staticmethod
	def get_shop_by_id(id):
		out_shop = {}
		_shop = ShopDB.by_id(id)
		if(_shop):
			out_shop = {
				"id": _shop.id,
				"title": _shop.title,
				"descript": _shop.descript,
				"items": [],
				"user_id": _shop.user_id
			}
		return out_shop


