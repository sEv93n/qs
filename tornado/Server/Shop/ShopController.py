#-*-coding: utf8

import tornado.template
from Server.lib.BaseHandler import BaseHandler
from Server.User.UserModel import UserModel
from Server.lib.AccessRule import AccessRule
from os import path
from re import findall
from Server.Shop.ShopModel import ShopModel

class ShopHandler(BaseHandler):

	@tornado.web.authenticated
	def act_get(self,url):
		self.render(	"shop.index.html",
						user_mode=self.user.types,
						menu=AccessRule(self).get_menu(),
						user_id=self.get_current_user(),
						**self.settings)

	@tornado.web.authenticated
	def act_get_add_shop(self,url):
		self.render(	"shop.add.html",
						user_mode=self.user.types,
						menu=AccessRule(self).get_menu(),
						user_id=self.get_current_user(),
						**self.settings)

	@tornado.web.authenticated
	def act_get_list(self,url):
		_list = ShopModel.list()
		self.json(_list)

	@tornado.web.authenticated
	def act_get_one(self,url):
		url=url[1:]
		if(len(url)>0):
			for u in url:
				r = findall(r'^(\d+)', u)
				if(len(r)>0):
					_shop = ShopModel.get_shop_by_id(int(r[0]))
					_shop['isadmin'] = self.user.id == _shop['user_id']
					self.json(_shop)
					return 0
		

	@tornado.web.authenticated
	def act_post(self,url):
		data = self.data_from_body_request()
		shop_id = data.get('id', None)
		if(not shop_id):
			_shop = ShopModel.create(data)
			self.json({
					"id":_shop.id,
					"title":_shop.title,
					"user_id":_shop.user.id,
					"user_name":"%s %s"%(_shop.user.fname, _shop.user.lname),
					"descript":_shop.descript
				})

