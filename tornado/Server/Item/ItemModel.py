#-*-coding: utf8

from os import popen,path
from hashlib import sha224
from shutil import copyfile
from PIL import Image

from Server.lib.SQLAlchemy_Main import ItemDB
from Server.Shop.ShopModel import ShopModel

class ItemModel:
	@staticmethod
	def  create_item(data,user):
		_shop = ShopModel.get_shop_by_id(data.get('shop_id',0))
		if(_shop and user.id == _shop['user_id']):
			_item = ItemDB.create_new(data)
			if(_item):
				return {
					"shop_id": _item.shop.id,
					"title": _item.title,
					"descript": _item.descript,
					"pricea": _item.price_a,
					"priceb": _item.price_b,
					"id": _item.id
				}
		else:
			return None

	@staticmethod
	def save_img(path,f):
		if(f['content_type'] == 'image/jpeg'):
			open(path,'wb').write(f['body'])

	@staticmethod
	def get_by_id(id):
		return ItemDB.by_id(id)
			
