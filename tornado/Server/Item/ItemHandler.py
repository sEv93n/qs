#-*-coding: utf8

import tornado.template
from Server.lib.BaseHandler import BaseHandler
from Server.lib.AccessRule import AccessRule
from os import path
from re import findall

from Server.Item.ItemModel import ItemModel

class ItemHandler(BaseHandler):
	@tornado.web.authenticated
	def act_get_new(self,url):
		url=url[1:]
		if(len(url)>0):	
			sid = findall(r'(\d+)',url[0])
			if(len(sid)>0):
				self.render(	"shop.item.add.html",
								user_mode=self.user.types,
								menu=AccessRule(self).get_menu(),
								user_id=self.get_current_user(),
								sid=sid[0],
								**self.settings)

	@tornado.web.authenticated
	def act_post_new(self,url):
		data = self.data_from_body_request()
		_item = ItemModel.create_item(data,self.user)
		self.json(_item)


	@tornado.web.authenticated
	def act_post_upload_file(self,url):
		f = self.request.files.get('file',False)
		item_id = self.get_argument('item_id',False)
		item = ItemModel.get_by_id(int(item_id))
		
		#TODO: CHECK PRIVILEGES
		
		res = False
		if(f and item):
			path_f = '%s/%d/%d/cover.jpg'%(
					self.settings['data_path'],
					item.shop_id,
					item.id
				)
			res = ItemModel.save_img(path,f[0])

		self.json({
			"result": res
		})







