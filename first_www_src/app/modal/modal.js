// Модуль нотификации
app.factory('Modals', ['$rootScope', '$http',
    function($rootScope,$http) {
        var _Modal = {
            base_path: '/app/modal/htmls/',
            status: '',
            content: undefined,
            show_modal: function(content){
                var $this = this;
                $this.content = content;
                $this.status = 'open';
            },
            open: function(modal_name){
                var $this = this;
                $http({
                    method: 'GET',
                    url: $this.base_path+modal_name
                }).success(function(data, status, headers, config) {
                    $this.show_modal(data);
                });
            }
        }
        return _Modal;
    }
]);
