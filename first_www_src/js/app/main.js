var app = angular.module('quickstart', ['ngResource', 'ngRoute', 'ngSanitize']);
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

function GlobalController($scope, $http) {
    // $scope.menu = srv.get_menu();
    // $scope.balans = srv.get_balans();
    $http({
      method: 'GET',
      url: '/user/info'
    }).success(function(data, status, headers, config) {
      if (data) {
          $scope.balans = data.balans;
          console.log($scope.balans);
      } else {
          console.log('FAIL');
      }
    }).error(function(data, status, headers, config) {});

}

function StartPage($scope, $http){
	$scope.modal = {
		data:{},
		send_auth: function(){
			var $this = this;
			$http({
                method: 'POST',
                url: '/auth',
                data: $this.data
            }).success(function(data, status, headers, config) {
                if (data.result) {
                    document.location.href = "/";
                } else{
                	console.log('FAIL');
                }
            }).error(function(data, status, headers, config) {
            });
		},
		send_reg: function(){
			var $this = this;
			$http({
                method: 'POST',
                url: '/reg',
                data: $this.data
            }).success(function(data, status, headers, config) {
                if (data.result) {
                    document.location.href = "/";
                } else{
                	console.log('FAIL');
                }
            }).error(function(data, status, headers, config) {
            });
		}
	};
}
